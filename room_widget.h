#ifndef ROOM_WIDGET_H
#define ROOM_WIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QMenu>

#include "room.h"
#include "zork_game.h"

class RoomWidget : public QWidget
{
    Q_OBJECT
private:
    Game* game;
    Room* current_room;

    void DrawRoomItems(QPainter& painter);
public slots:
    void ShowContextMenu(const QPoint& pos);
protected:
    void paintEvent(QPaintEvent*);

public:
    RoomWidget(Game* game, QWidget* parent = nullptr);
    virtual ~RoomWidget() {}

    void SetRoom(Room* new_room);
};

#endif // ROOM_WIDGET_H
