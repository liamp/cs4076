#-------------------------------------------------
#
# Project created by QtCreator 2018-09-25T14:23:39
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = zork
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    zork_game.cc \
    room.cc \
    room_widget.cc \
    player.cpp \
    item.cc \
    inventory_widget.cc \
    keypad_widget.cpp \
    prop.cc \
    game_object.cc \
    the_wall.cc \
    teleporter.cc

HEADERS += \
        mainwindow.h \
    zork_game.h \
    room.h \
    room_widget.h \
    player.h \
    item.h \
    player.h \
    inventory_widget.h \
    math_utils.h \
    keypad_widget.h \
    prop.h \
    game_object.h \
    the_wall.h \
    teleporter.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resrc.qrc

DISTFILES += \
    CrackedCup.png
