#include <iostream>
#include "zork_game.h"

#include <QMessageBox>
#include "keypad_widget.h"
#include "prop.h"
#include "the_wall.h"
#include "teleporter.h"


using MathUtils::vec2;
using std::string;

const QString Game::SecretRoomCode = "3405691582"; // 0xcafebabe in decimal
const QString Game::VictoryCode = "8014";

// Raw string literals for no reason whatsoever.
static const string EricRamble = R"LOL(Whenever Sonic beats Eggman in the final level, Eggman always outruns him and gets to his giant robot before Sonic catches him, beating Sonic's average running speed of 767.269 miles per hour which is the speed of sound. This means that Dr. Eggman is actually the fastest thing alive.)LOL";

Game::Game()
{
    this->player = new Player();
}

Player*
Game::GetPlayer()
{
    return this->player;
}

void
Game::LoadRooms()
{
    const QPixmap torch_sprite = QPixmap(":/data/torch.png");
    const QPixmap cup_sprite = QPixmap(":/data/CrackedCup.png");
    const QPixmap flashlight_sprite = QPixmap(":/data/Flashlight.png");
    const QPixmap rug_sprite = QPixmap(":/data/Rug.png");
    const QPixmap skeleton_sprite = QPixmap(":/data/Skeleton.png");
    const QPixmap battery_sprite = QPixmap(":/data/Battery.png");
    const QPixmap bed_sprite = QPixmap(":/data/Bed.png");
    const QPixmap desk_sprite = QPixmap(":/data/Desk.png");
    const QPixmap chair_sprite = QPixmap(":/data/Chair.png");
    const QPixmap crack_sprite = QPixmap(":/data/Crack.png");
    const QPixmap wall_sprite = QPixmap(":/data/wall.png");
    const QPixmap emptychest_sprite = QPixmap(":/data/EmptyChest.png");
    const QPixmap liam_sprite = QPixmap(":/data/Liam.png");
    const QPixmap eric_sprite = QPixmap(":/data/Eric.png");
    const QPixmap kyran_sprite = QPixmap(":/data/Kyran.png");

    Room *a, *b, *c, *victory_room, *secret_room;
    a = new Room("Room A");
    b = new Room("Room B");
    c = new Room("Room C");
    victory_room = new Room("Victory Room");
    secret_room = new Room("Secret Room");

    //                  North    South    East     West
    a->SetAdjacentRooms(b,       nullptr, nullptr, nullptr);
    b->SetAdjacentRooms(nullptr, a,       secret_room, c);
    c->SetAdjacentRooms(nullptr, nullptr, b,       victory_room);
    victory_room->SetAdjacentRooms(nullptr, nullptr, c,       nullptr);
    secret_room->SetAdjacentRooms(nullptr, nullptr, nullptr,       b);

    a->AddObject(new Item("Torch", "A torch", vec2<int>{100, 100}, flashlight_sprite));
    a->AddObject(new Item("Batteries", "Some Batteries", vec2<int>{100, 200}, battery_sprite));
    a->AddObject(new Prop("Crack in the floor", "A crack in the floor", vec2<int>{460,300}, crack_sprite));

    b->AddObject(new TheWall(vec2<int>{460, 300}, vec2<int>{30, 30}, torch_sprite));
    b->AddObject(new Item("Cracked Glass","Cracked glass cup. Worthless",vec2<int>{130, 130}, cup_sprite));
    b->AddObject(new Item("Empty Chest", "Old chest, Completely empty", vec2<int>{450,110}, emptychest_sprite));

    c->AddObject(new HiddenMessage(vec2<int>{400,100}, torch_sprite, "Code is 0xCAFEBABE"));
    c->AddObject(new Prop("Skeleton", "There is a skeleton. Spooky", vec2<int>{100, 450}, skeleton_sprite));
    c->AddObject(new Item("Fine Rug", "Strange, what would such fine rug be doing here?", vec2<int>{250,250}, rug_sprite));

    secret_room->AddObject(new Prop("Kyran's Bed", "Looks well used", vec2<int>{110, 450}, bed_sprite));
    secret_room->AddObject(new Prop("Liam's Chair", "Worn cloth chair. Bears the inscription: \"Days since last segfault - approx. 0.5\"", vec2<int>{300, 125}, chair_sprite));
    secret_room->AddObject(new Prop("Eric's Chair", "Worn cloth chair. Bears the initials: \"K.T.\"",vec2<int>{400,125}, chair_sprite));
    secret_room->AddObject(new Prop("Liam's Desk", "Monitor reads: " + std::string(__FUNCTION__), vec2<int>{300,150}, desk_sprite));
    secret_room->AddObject(new Prop("Eric's Desk", "Monitor reads: git: fatal error, you have work that is not merged into upstream. Commit your changes first.", vec2<int>{400,150}, desk_sprite));
    secret_room->AddObject(new Prop("Liam", "Hey! How did you get in here!? The game isn't even done yet!", vec2<int>{265,150},liam_sprite));
    secret_room->AddObject(new Prop("Eric", EricRamble, vec2<int>{435,150}, eric_sprite));
    secret_room->AddObject(new Prop("Kyran", "ZZZZZZZZZ", vec2<int>{110,430},kyran_sprite));

    victory_room->SetLocked(Direction::West, Game::VictoryCode.toStdString());
    secret_room->SetLocked(Direction::East, Game::SecretRoomCode.toStdString());

    this->loaded_rooms.push_back(a);
    this->loaded_rooms.push_back(b);
    this->loaded_rooms.push_back(c);
    this->loaded_rooms.push_back(victory_room);
    this->loaded_rooms.push_back(secret_room);
}

void
Game::Start()
{
    LoadRooms();
    this->current_room = this->loaded_rooms[0];
}

void
Game::GoToRoom(Direction exit_direction)
{
    Room* adjacent = this->current_room->GetAdjacent(exit_direction);
    if (adjacent)
    {
        if (!adjacent->IsLocked(exit_direction))
        {
            this->current_room = adjacent;
        }
        else
        {
            Keypad keypad(adjacent->GetCode().c_str());

            if(keypad.AttemptUnlock() == true)
            {
                adjacent->Unlock();
                this->current_room = adjacent;
                if (adjacent->GetName() == "Victory Room")
                {
                    QMessageBox msg;
                    msg.setText("You win!");
                    msg.exec();
                }
            }
        }
    }
}

void
Game::GoToRoom(Room* new_room)
{
    this->current_room = new_room;
}

Room*
Game::GetCurrentRoom() const
{
    return this->current_room;
}

Game::~Game()
{
    for (auto & loaded_room : this->loaded_rooms)
    {
        delete loaded_room;
    }

    delete this->player;
}
