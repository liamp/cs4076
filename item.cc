#include <iostream>
#include <QMessageBox>
#include "item.h"
#include "player.h"
#include "zork_game.h"
#include <QMetaMethod>

using MathUtils::vec2;
using std::string;


Item::Item(string name, string description, vec2<int> pos, QPixmap sprite) : GameObject (name, description, pos, sprite)
{
    this->inspect_action = new QAction("Inspect");
    this->take_action = new QAction("Take Item");
    this->sprite = sprite;
    this->connected = false;
}

const QPixmap&
Item::GetSprite() const
{
    return this->sprite;
}

void
Item::PickUp(Game* game_state)
{
    game_state->GetPlayer()->AddItem(this);
    game_state->GetCurrentRoom()->RemoveObject(this);
}

void
Item::ShowDescriptionBox()
{
    QMessageBox msg;
    msg.setText(QString(this->description.c_str()));
    msg.exec();
}

void
Item::GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state)
{
    menu->addAction(this->inspect_action);
    menu->addAction(this->take_action);

    // Stupid hack
    if (!this->connected)
    {
        parent->connect(this->take_action, &QAction::triggered, [this, game_state, parent]() {
            this->PickUp(game_state);
            parent->parentWidget()->repaint();
            parent->parentWidget()->update();
        });
        parent->connect(this->inspect_action, &QAction::triggered, [this]() { this->ShowDescriptionBox();});

        this->connected = true;
    }
}

Item::~Item()
{
    delete this->inspect_action;
    delete this->take_action;
}
