#ifndef PROP_H
#define PROP_H

#include <string>
#include <QMenu>

#include "math_utils.h"
#include "game_object.h"

// Represents a game object that can only be inspected, not picked up or
// interacted with in any other way.
class Prop : public GameObject
{
private:
    QAction* inspect_action;         // Hook for the inspect context menu action (N.B. destroyed by this class!)
    bool     connected;              // Lame hack we have to use here to stop duplicate Qt Connections.
public:
    Prop(std::string name, std::string description, MathUtils::vec2<int> pos, QPixmap sprite, MathUtils::vec2<int> dimensions = { 30, 30 });
    ~Prop() override;
    void GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game) override;
    void ShowDescriptionBox() const;
};

#endif // PROP_H
