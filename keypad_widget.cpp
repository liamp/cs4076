#include <QPushButton>
#include <QGridLayout>
#include <QBoxLayout>
#include <QWidget>
#include <QLineEdit>
#include <iostream>
#include <QMessageBox>

#include "keypad_widget.h"

const QString Keypad::WINNING_CODE = "8014";

using std::string;

Keypad::Keypad(QString code, QDialog *parent): QDialog(parent)
{
    this->grid_layout = new QGridLayout();

    this->code = code;
    keypad_win.resize(300,300);
    keypad_win.setLayout(grid_layout);

    this->dialog = new QLineEdit(this);
    this->is_code_correct = false;

    this->ClearInput();
    this->button_1 = new QPushButton("1");
    this->button_2 = new QPushButton("2");
    this->button_3 = new QPushButton("3");
    this->button_4 = new QPushButton("4");
    this->button_5 = new QPushButton("5");
    this->button_6 = new QPushButton("6");
    this->button_7 = new QPushButton("7");
    this->button_8 = new QPushButton("8");
    this->button_9 = new QPushButton("9");
    this->button_0 = new QPushButton("0");
    this->button_enter = new QPushButton("Enter");
    this->button_clear = new QPushButton("Clear");

    grid_layout->addWidget(dialog,0,0,1,3);
    grid_layout->addWidget(button_1,1,0,1,1);
    grid_layout->addWidget(button_2,1,1,1,1);
    grid_layout->addWidget(button_3,1,2,1,1);
    grid_layout->addWidget(button_4,2,0,1,1);
    grid_layout->addWidget(button_5,2,1,1,1);
    grid_layout->addWidget(button_6,2,2,1,1);
    grid_layout->addWidget(button_7,3,0,1,1);
    grid_layout->addWidget(button_8,3,1,1,1);
    grid_layout->addWidget(button_9,3,2,1,1);
    grid_layout->addWidget(button_0,4,1,1,1);
    grid_layout->addWidget(button_enter,4,2,1,1);
    grid_layout->addWidget(button_clear,4,0,1,1);

    connect(button_1, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("1");});
    connect(button_2, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("2");});
    connect(button_3, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("3");});
    connect(button_4, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("4");});
    connect(button_5, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("5");});
    connect(button_6, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("6");});
    connect(button_7, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("7");});
    connect(button_8, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("8");});
    connect(button_9, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("9");});
    connect(button_0, &QPushButton::released,this,[this]() {this->OnNumpadButtonPressed("0");});
    connect(button_enter, &QPushButton::released,this, &Keypad::EnterPressed);
    connect(button_clear, &QPushButton::released,this, &Keypad::ClearInput);
}

bool
Keypad::AttemptUnlock()
{
   keypad_win.exec();
   return this->is_code_correct;
}

void
Keypad::OnNumpadButtonPressed(QString number)
{
    current_text = current_text + number;
    dialog->setText(current_text);
}

void
Keypad::ClearInput()
{
    this->dialog->setText("");
    this->current_text = "";
}

void
Keypad::EnterPressed()
{
    if(current_text == this->code)
    {
        this->is_code_correct = true;
        keypad_win.close();
    }
    else
    {
        this->is_code_correct = false;
        QMessageBox msg;
        msg.setText("Incorrect Code");
        msg.exec();
    }
}

Keypad::~Keypad()
{
    delete this->button_0;
    delete this->button_1;
    delete this->button_2;
    delete this->button_3;
    delete this->button_4;
    delete this->button_5;
    delete this->button_6;
    delete this->button_7;
    delete this->button_8;
    delete this->button_9;
    delete this->button_clear;
    delete this->button_enter;
    delete this->grid_layout;
    delete this->dialog;
}
