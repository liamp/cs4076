#ifndef OBSERVER_H
#define OBSERVER_H

template<typename T>
class Observer
{
public:
    virtual void OnChange(T& subject) = 0;
    Observer() {}
    virtual ~Observer() {}
};

#endif // OBSERVER_H
