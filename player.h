#ifndef PLAYER_H
#define PLAYER_H
#include <vector>
#include <string>

class Item;

class Player
{
private:
    std::vector<Item*> inventory;
public:
    std::vector<Item*> GetInventory() const;
    void AddItem(Item*);
    void DeleteItem(Item*);
    bool AttemptItemCombination(Item* A, Item* B);
    bool HasItemByName(const std::string& item_name) const;
    Player();
    ~Player();
};

#endif // PLAYER_H
