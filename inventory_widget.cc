#include <QMenu>
#include <QPainter>
#include <QInputDialog>
#include <QMouseEvent>
#include <QMessageBox>
#include "inventory_widget.h"
#include "player.h"
#include "item.h"
#include "math_utils.h"

#define ITEM_NAME_RECT(index) QRect(100, (100 + ((index) * 20)), 100, 20)

using std::vector;

static const QBrush inventory_color = QColor(230, 0, 0);


InventoryWidget::InventoryWidget(Player* player ,QWidget* parent) : QWidget(parent)
{
    this->player = player;
    this->setFixedSize(400, 600);
}


void
InventoryWidget::mousePressEvent(QMouseEvent* event)
{
    int item_index = 0;
    for (Item* current_item : this->player->GetInventory())
    {
        QRect item_name_bounds = ITEM_NAME_RECT(item_index);
        if (MathUtils::PointWithinBounds(event->pos(), item_name_bounds))
        {
            // User has selected a valid item. If the previously selected item
            // index is valid, attempt combine of both these items. If not, set
            // the item index to this item.
            if (this->selected_item_index != -1)
            {
                Item* prev_selected_item = this->player->GetInventory().at(this->selected_item_index);
                QMessageBox msg;
                if (this->player->AttemptItemCombination(prev_selected_item, current_item))
                {
                    // Combination was successful -> reset selected index to -1
                    // TODO(liam|eric): Maybe we don't need to show a message box here? (feels snappier without)
                    msg.setText("Combination successful!");
                    msg.exec();
                }
                else
                {
                    msg.setText("That combination did not work!");
                    msg.exec();
                }

                this->selected_item_index = -1;

            }
            else
            {
                this->selected_item_index = item_index;
            }

            this->parentWidget()->repaint();
            this->parentWidget()->update();
        }

        ++item_index;
    }
}

void InventoryWidget::paintEvent(QPaintEvent*)
{
    QPainter renderer(this);

    renderer.setBrush(inventory_color);
    renderer.setFont(QFont("Arial",10));
    renderer.setPen(Qt::red);
    renderer.drawText(100, 100, "Inventory");
    auto inventory = this->player->GetInventory();
    renderer.setPen(Qt::magenta);
    for (vector<Item*>::size_type i = 0; i != inventory.size(); ++i)
    {
        const int offset = static_cast<int>(i);
        if (offset == this->selected_item_index)
        {
            renderer.setBrush(Qt::blue);
            renderer.drawRect(ITEM_NAME_RECT(offset));
        }

        renderer.drawText(ITEM_NAME_RECT(offset), Qt::AlignLeft, inventory[i]->GetName().c_str());
    }
}


