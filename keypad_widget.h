#ifndef KEYPAD_H
#define KEYPAD_H
#include <QPushButton>
#include <QWidget>
#include <QTextEdit>
#include <QGridLayout>
#include <QLineEdit>
#include <QDialog>

class Keypad : public QDialog
{
    Q_OBJECT
public:
    explicit Keypad(QString code, QDialog *parent = nullptr);
    ~Keypad();
    bool AttemptUnlock();
    QDialog keypad_win;

    static const QString WINNING_CODE;
private slots:
    void OnNumpadButtonPressed(QString);
    void EnterPressed();
private:
    QLineEdit *dialog;
    QGridLayout *grid_layout;
    QPushButton *button_1;
    QPushButton *button_2;
    QPushButton *button_3;
    QPushButton *button_4;
    QPushButton *button_5;
    QPushButton *button_6;
    QPushButton *button_7;
    QPushButton *button_8;
    QPushButton *button_9;
    QPushButton *button_0;
    QPushButton *button_enter;
    QPushButton *button_clear;

    QString current_text;
    bool is_code_correct;
    QString code;

    void ClearInput();

};

#endif // KEYPAD_H
