#include <QMessageBox>
#include "teleporter.h"
#include "zork_game.h"

using MathUtils::vec2;
using std::string;

HiddenMessage::HiddenMessage(vec2<int> pos, QPixmap sprite, string secret_msg) : GameObject ("Teleporter", "A strange device disguised as a wall lamp", pos, sprite)
{
    this->connected_inspect = false;
    this->connected_pull = false;
    this->inspect_action = new QAction("Inspect");
    this->pull_action = new QAction("Activate");
    this->secret_msg = secret_msg;
}


void
HiddenMessage::ShowDescriptionBox()
{
    QMessageBox msg;
    msg.setText(QString(this->description.c_str()));
    msg.exec();
}

void
HiddenMessage::ShowSecretMessage()
{
    QMessageBox msg;
    msg.setText(QString(this->secret_msg.c_str()));
    msg.exec();
}

void
HiddenMessage::GetActionsOnClick(QMenu* menu, QWidget* parent, Game*)
{
    menu->addAction(this->inspect_action);

    if (!this->connected_inspect)
    {
        parent->connect(this->inspect_action, &QAction::triggered, [this]() { this->ShowDescriptionBox();});
        this->connected_inspect = true;
    }
    else // Show the activation if we've already inspected it
    {
        menu->addAction(this->pull_action);
        if (!this->connected_pull)
        {
            parent->connect(this->pull_action, &QAction::triggered, [this]() {this->ShowSecretMessage(); });
            this->connected_pull = true;
        }
    }
}

HiddenMessage::~HiddenMessage()
{
    delete this->inspect_action;
    delete this->pull_action;
}
