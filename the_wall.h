#ifndef THE_WALL_H
#define THE_WALL_H

#include "math_utils.h"
#include "game_object.h"

class TheWall : public GameObject
{
private:
    QAction* inspect_action;
    QAction* shine_action;
    bool     connected_inspect;
    bool     connected_shine;

    void ShowDescriptionBox();
    void ShowSecretMessageBox();
public:
    TheWall(MathUtils::vec2<int> pos, MathUtils::vec2<int> dimensions, QPixmap sprite);
    ~TheWall() override;

    void GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state) override;

};

#endif // THE_WALL_H
