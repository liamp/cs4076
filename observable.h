#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include <vector>
#include <algorithm>

#include "observer.h"

template<typename T>
class Observable
{
private:
    std::vector<Observer<T>*> observers;
public:
    void Attach(Observer<T>* o)
    {
        this->observers.push_back(o);
    }

    void Detach(Observer<T>* o)
    {
        // TODO Implement later
        //auto it = std::find(this->observers.begin(), this->observers.end(), o);
        //this->observers.erase(this->observers.begin() + it);
    }

    void NotifyObservers() const
    {
        // Loops over all of the registered observers and calls their OnChange function
        // with the current observable class as data. A C++11 Lambda expression is used
        // to call the update function. The code is equivalent to the following pseudocode:
        //
        //     foreach (observer in observers):
        //         observer.OnChange(this) <-- lambda expression equivalent
        //
        std::for_each(
            this->observers.begin(),
            this->observers.end(),
            [this](Observer<T>* o) { o->OnChange(this); }
        );

    }
};

#endif // OBSERVABLE_H
