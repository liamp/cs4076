#include "game_object.h"
#include "zork_game.h"

using std::string;
using MathUtils::vec2;

GameObject::GameObject(string name, string description, vec2<int> pos, QPixmap sprite, vec2<int> dimensions)
{
    this->name = name;
    this->description = description;
    this->pos = pos;
    this->bounding_box = QRect(pos.X, pos.Y, dimensions.X, dimensions.Y);
    this->sprite = sprite;
}

const string&
GameObject::GetName() const
{
    return this->name;
}

const string&
GameObject::GetDescription() const
{
    return this->description;
}

vec2<int>
GameObject::GetPosition() const
{
    return this->pos;
}

QRect
GameObject::GetBoundingBox() const
{
    return this->bounding_box;
}

const QPixmap&
GameObject::GetSprite() const
{
    return this->sprite;
}
