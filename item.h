#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <QRectF>
#include <QMetaType>
#include <QPixmap>
#include "math_utils.h"
#include "game_object.h"

// Forward declaration of class Player
class Player;

class Item : public GameObject
{
private:
    QAction* take_action;
    QAction* inspect_action;
    bool connected;

    void ShowDescriptionBox();
    void PickUp(Game* game_state);
public:
    Item(std::string name, std::string description, MathUtils::vec2<int> pos, QPixmap sprite);
    ~Item() override;

    void GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state) override;
    const QPixmap& GetSprite() const;
};

#endif // ITEM_H
