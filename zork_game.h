#ifndef ZORK_GAME_H
#define ZORK_GAME_H

#include <vector>
#include "room.h"
#include "player.h"

class Game
{
private:
    Room*   current_room;
    std::vector<Room*> loaded_rooms;
    Player* player;

    static const QString VictoryCode;
    static const QString SecretRoomCode;

    void QuitGame();
    void LoadRooms();
public:
    Game();
    ~Game();

    void  GoToRoom(Direction exit_direction);
    void  GoToRoom(Room* new_room);
    void  Start();
    Room* GetCurrentRoom() const;
    Player* GetPlayer();
};


#endif // ZORK_GAME_H
