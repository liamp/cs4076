#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QPainter>
#include <QGridLayout>
#include <iostream>
#include <QBoxLayout>

MainWindow::MainWindow(Game* game, QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->layout = new QGridLayout(nullptr);

    this->main_widget = new QWidget;

    north_button = new QPushButton("North Button", this);
    south_button = new QPushButton("South Button",this);
    west_button = new QPushButton("West Button",this);
    east_button = new QPushButton("East Button",this);
    north_button->setGeometry(QRect(QPoint(100,450),QSize(150,50)));
    south_button->setGeometry(QRect(QPoint(100,500),QSize(150,50)));
    west_button->setGeometry(QRect(QPoint(100,550),QSize(150,50)));
    east_button->setGeometry(QRect(QPoint(100,600),QSize(150,150)));
    this->game = game;
    game->Start();

    this->layout->setContentsMargins(0, 0, 0, 0);
    this->inv_widget = new InventoryWidget(this->game->GetPlayer(), this);
    this->map_widget = new RoomWidget(game, this);
    layout->addWidget(north_button, 3, 1);
    layout->addWidget(south_button, 4, 1);
    layout->addWidget(east_button, 3, 2);
    layout->addWidget(west_button, 4, 2);
    layout->addWidget(map_widget, 2, 1);
    layout->addWidget(inv_widget, 2, 2);
    main_widget->setLayout(layout);
    this->setCentralWidget(main_widget);


    north_button->setText("Go North");
    south_button->setText("Go South");
    west_button->setText("Go West");
    east_button->setText("Go East");

    connect(north_button,SIGNAL(released()),this,SLOT(NorthButtonClick()));
    connect(south_button,SIGNAL(released()),this,SLOT(SouthButtonClick()));
    connect(west_button,SIGNAL(released()),this,SLOT(WestButtonClick()));
    connect(east_button,SIGNAL(released()),this,SLOT(EastButtonClick()));
}

void MainWindow::NorthButtonClick()
{
    this->game->GoToRoom(Direction::North);
    this->map_widget->SetRoom(this->game->GetCurrentRoom());
    this->map_widget->repaint();
    this->map_widget->update();
}

void MainWindow::SouthButtonClick()
{
    this->game->GoToRoom(Direction::South);
    this->map_widget->SetRoom(this->game->GetCurrentRoom());
    this->map_widget->repaint();
    this->map_widget->update();
}
void MainWindow::WestButtonClick()
{
    this->game->GoToRoom(Direction::West);
    this->map_widget->SetRoom(this->game->GetCurrentRoom());
    this->map_widget->repaint();
    this->map_widget->update();
}
void MainWindow::EastButtonClick()
{
    this->game->GoToRoom(Direction::East);
    this->map_widget->SetRoom(this->game->GetCurrentRoom());
    this->map_widget->repaint();
    this->map_widget->update();
}


MainWindow::~MainWindow()
{
    delete ui;
    delete map_widget;
    delete inv_widget;
    delete layout;
    delete main_widget;
}
