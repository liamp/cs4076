#ifndef INVENTORY_WIDGET_H
#define INVENTORY_WIDGET_H
#include <QWidget>
#include "player.h"


class InventoryWidget : public QWidget
{
    Q_OBJECT
private:
    Player* player;

    // Used for item combination logic. The user may select an item in the list
    // to combine another item with. A value of -1 indicates that the user has not
    // selected an initial item to combine with.
    int selected_item_index = -1;

protected:
    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent*);

public:
    InventoryWidget(Player* player, QWidget* parent = nullptr);
};

#endif // INVENTORY_WIDGET_H
