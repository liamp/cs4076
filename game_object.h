#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <string>
#include <QMenu>
#include <QWidget>
#include <QPixmap>

#include "math_utils.h"

// Forward declaration of Game class to prevent cyclic header includes.
class Game;


// Represents an object in the game. An object:
// - Exists as a physical object in the room
// - Has a sprite image as its avatar
// - Can be right clicked on to interact with
class GameObject
{
protected:
    std::string name;             // String name for the object (used as ID)
    std::string description;      // Description (used by subclasses on infobox popup)
    QRect     bounding_box;       // Physical bounding box of the object. Starts from `pos`
    MathUtils::vec2<int> pos;     // Physical origin position of the object.
    QPixmap sprite;               // Image to draw for the sprite (N.B. QPixmaps have value semantics (I think))
public:
    GameObject(std::string name, std::string description, MathUtils::vec2<int> pos, QPixmap sprite, MathUtils::vec2<int> dimensions = { 30, 30 });
    QRect GetBoundingBox() const;
    MathUtils::vec2<int> GetPosition() const;
    const std::string& GetName() const;
    const std::string& GetDescription() const;
    const QPixmap& GetSprite() const;

    // Fired when the object is right-clicked on in the world.
    // This method should add all relevant connections and actions into the `menu` param.
    virtual void GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state) = 0;

    // C++11 =default syntax here (equivalent to `virtual GameObject() {}`)
    virtual ~GameObject() = default;
};


#endif // GAME_OBJECT_H
