#include "room.h"


using std::string;
using std::vector;

Room::Room(string name)
{
    this->name = name;
    this->locked[int(Direction::North)] = false;
    this->locked[int(Direction::South)] = false;
    this->locked[int(Direction::East)] = false;
    this->locked[int(Direction::West)] = false;
}

Room*
Room::GetAdjacent(Direction exit_direction) const
{
    return this->exits[int(exit_direction)];

}

const string&
Room::GetName() const
{
    return this->name;
}

void
Room::SetAdjacentRooms(Room* north, Room* south, Room* east, Room* west)
{
    this->exits[int(Direction::North)] = north;
    this->exits[int(Direction::South)] = south;
    this->exits[int(Direction::East)] = east;
    this->exits[int(Direction::West)] = west;
}

vector<GameObject*>
Room::GetLoadedObjects() const
{
    return this->loaded_objects;
}

void
Room::AddObject(GameObject* item)
{
    this->loaded_objects.push_back(item);
}


void
Room::SetLocked(Direction d, string code)
{
    this->locked[int(d)] = true;
    this->locked_code = code;
}

bool
Room::IsLocked(Direction d) const
{
    return this->locked[int(d)];
}

void
Room::RemoveObject(GameObject* object)
{
    auto pos = std::find(this->loaded_objects.begin(), this->loaded_objects.end(), object);
    if (pos != this->loaded_objects.end())
    {
        this->loaded_objects.erase(pos);
    }
}

void
Room::Unlock()
{
    this->locked[0] = false;
    this->locked[1] = false;
    this->locked[2] = false;
    this->locked[3] = false;
}

const string&
Room::GetCode()
{
    return this->locked_code;
}

Room::~Room()
{
    for (GameObject* object : this->loaded_objects)
    {
        delete object;
    }

    this->loaded_objects.clear();
}
