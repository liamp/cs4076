#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QBoxLayout>

#include <string>

#include "zork_game.h"
#include "room_widget.h"
#include "inventory_widget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    Game*   game;
    QWidget* main_widget;
    RoomWidget*  map_widget;
    InventoryWidget* inv_widget;
    QGridLayout* layout;
    QPushButton *north_button;
    QPushButton *south_button;
    QPushButton *west_button;
    QPushButton *east_button;

public:
    explicit MainWindow(Game* game, QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void NorthButtonClick();
    void SouthButtonClick();
    void WestButtonClick();
    void EastButtonClick();

};

#endif // MAINWINDOW_H
