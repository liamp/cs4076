#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <QRect>
#include <QPoint>

namespace MathUtils
{
// Custom template class for a 2-vector of any type.
// This exists _purely_ because I couldn't think of
// any way to shoehorn templates into the actual Zork
// code (attempts at a compile-time Room graph nonwithstanding...)
// So here it is: the 2-vector reinvented for the umpteenth time.
// 
// A struct is used here because this is a just a simple POD type;
// there is no real difference between `struct` and `class` in C++,
// the only distinction is that members are private by default in a 
// class and public by default in a struct.
template<typename T>
struct vec2
{
    T X;
    T Y;
};

// Overload some operators because why not
template<typename T> inline
vec2<T> operator+(const vec2<T>& A, const vec2<T>& B)
{
    vec2<T> Result;
    Result.X = A.X + B.X;
    Result.Y = A.Y + B.Y;

    return Result;
}

template<typename T> inline
vec2<T> operator-(const vec2<T>& A, const vec2<T>& B)
{
    vec2<T> Result;
    Result.X = A.X - B.X;
    Result.Y = A.Y - B.Y;

    return Result;
}


inline bool
operator<=(const QPointF& a, const QPointF& b)
{
    return a.x() <= b.x() && a.y() <= b.y();
}

inline bool
operator>=(const QPointF& a, const QPointF& b)
{
    return a.x() >= b.x() && a.y() >= b.y();
}

inline bool
operator<=(const QPoint& a, const QPoint& b)
{
    return a.x() <= b.x() && a.y() <= b.y();
}

inline bool
operator>=(const QPoint& a, const QPoint& b)
{
    return a.x() >= b.x() && a.y() >= b.y();
}

// Tests if a particular point is contained within the
// specified rectangle.
inline bool
PointWithinBounds(const QPointF& point, const QRectF& bounds)
{
    return (bounds.topLeft() <= point) && (bounds.bottomRight() >= point);
}

// Overloaded for convenience with integer operations.
inline bool
PointWithinBounds(const QPoint& point, const QRect& bounds)
{
    return (bounds.topLeft() <= point) && (bounds.bottomRight() >= point);
}

} // namespace MathUtils

#endif // MATH_UTILS_H
