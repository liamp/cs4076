#include <QMessageBox>
#include "zork_game.h"
#include "the_wall.h"
#include "keypad_widget.h"

using MathUtils::vec2;

TheWall::TheWall(vec2<int> pos, vec2<int> dimensions, QPixmap sprite) : GameObject("The Wall", "There is writing on the wall: Shine the light to escape the darkness", pos, sprite, dimensions)
{
    this->inspect_action = new QAction("Inspect");
    this->shine_action = new QAction("Shine Flashlight");
    this->connected_inspect = false;
    this->connected_shine = false;
}

void
TheWall::GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state)
{
    menu->addAction(this->inspect_action);
    if (!this->connected_inspect)
    {
        parent->connect(this->inspect_action, &QAction::triggered, [this] { this->ShowDescriptionBox(); });
        this->connected_inspect = true;
    }

    if (game_state->GetPlayer()->HasItemByName("Powered Torch"))
    {
        menu->addAction(this->shine_action);
        if (!this->connected_shine)
        {
            parent->connect(this->shine_action, &QAction::triggered, [this] { this->ShowSecretMessageBox(); });
            this->connected_shine = true;
        }
    }
}

void
TheWall::ShowSecretMessageBox()
{
    QMessageBox msg;
    msg.setText("The code is " + Keypad::WINNING_CODE);
    msg.exec();
}

void
TheWall::ShowDescriptionBox()
{
    QMessageBox msg;
    msg.setText(QString(this->description.c_str()));
    msg.exec();
}

TheWall::~TheWall()
{
    delete this->inspect_action;
    delete this->shine_action;
}
