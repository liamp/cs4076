#include "mainwindow.h"
#include "zork_game.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Game* game = new Game();
    MainWindow w(game);
    w.show();

    const int app_return = a.exec();
    delete game;

    return app_return;
}
