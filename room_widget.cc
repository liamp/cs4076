#include <QPainter>
#include <QPainterPath>
#include <QMouseEvent>
#include <QMessageBox>

#include <functional>
#include <iostream>

#include "room_widget.h"
#include "math_utils.h"
#include "item.h"
#include "prop.h"

using namespace MathUtils;

static const QBrush door_color = QColor(0x161616);
static const QBrush wall_color = QColor(127, 127, 127);
static const QBrush floor_color = QColor(64, 64, 64);
static const QBrush text_color = QColor(255, 0, 0);

RoomWidget::RoomWidget(Game* game, QWidget* parent) : QWidget(parent)
{
    this->game = game;
    this->current_room = game->GetCurrentRoom();
    this->setFixedSize(600, 600);
    this->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowContextMenu(const QPoint &)));
}

void RoomWidget::ShowContextMenu(const QPoint& pos)
{
    QMenu contextMenu("Actions", this);
    QAction inspect("Inspect", this);
    QAction take("Take Item", this);
    QAction shine("Shine light", this);

    for (auto object : this->current_room->GetLoadedObjects())
    {
        if (MathUtils::PointWithinBounds(pos, object->GetBoundingBox()))
        {
            object->GetActionsOnClick(&contextMenu, this, this->game);
            break; // Can stop iterating as soon as we've found something to act on
        }
    }

    contextMenu.exec(mapToGlobal(pos));
}

void
RoomWidget::SetRoom(Room* new_room)
{
   this->current_room = new_room;
}

void
RoomWidget::DrawRoomItems(QPainter& renderer)
{
    for (auto* object : this->current_room->GetLoadedObjects())
    {
        QPixmap img = QPixmap(object->GetSprite());
        QRect draw_rect = object->GetBoundingBox();
        renderer.drawPixmap(draw_rect, img);
    }
}

void RoomWidget::paintEvent(QPaintEvent*)
{
    QPainter renderer(this);
    renderer.setRenderHint(QPainter::Antialiasing);

    // Set up to draw the room's walls
    renderer.setBrush(wall_color);
    renderer.drawRect(QRect(100, 100, 400, 400));
    renderer.setBrush(floor_color);
    renderer.drawRect(QRect(105, 105, 390, 390));

    Room* north = this->current_room->GetAdjacent(Direction::North);
    Room* south = this->current_room->GetAdjacent(Direction::South);
    Room* east = this->current_room->GetAdjacent(Direction::East);
    Room* west = this->current_room->GetAdjacent(Direction::West);

    // Set the brush color to the background so it looks like there's gaps in the
    // walls for the doors.
    renderer.setBrush(door_color);
    if (north) renderer.fillRect(QRect(275, 100, 50, 5), door_color);
    if (south) renderer.fillRect(QRect(275, 495, 50, 5), door_color);
    if (east)  renderer.fillRect(QRect(495, 275, 5, 50), door_color);
    if (west)  renderer.fillRect(QRect(100, 275, 5, 50), door_color);

    this->DrawRoomItems(renderer);

    // Draw the room's name
    renderer.setPen(Qt::magenta);
    renderer.setFont(QFont("Arial", 16));
    renderer.drawText(QRect(100, 50, 400, 400), Qt::AlignHCenter, this->current_room->GetName().c_str());
}

