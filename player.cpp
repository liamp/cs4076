#include "player.h"
#include <string>
#include <vector>
#include <algorithm>

#include <QPixmap>
#include "item.h"
using std::vector;
using std::string;


vector<Item*>
Player::GetInventory() const
{
    return this-> inventory;
}

void
Player::AddItem(Item* i)
{
    this->inventory.push_back(i);
}

void
Player::DeleteItem(Item* i)
{
    this->inventory.erase(std::remove(this->inventory.begin(), this->inventory.end(), i));
}

Player::Player()
{
    this->inventory = vector<Item*>();
}

bool
Player::AttemptItemCombination(Item* A, Item* B)
{
    if ((A->GetName() == "Batteries" || B->GetName() == "Torch") && (A->GetName() == "Batteries" && B->GetName() == "Torch") && A != B)
    {
        this->AddItem(new Item("Powered Torch", "A torch with batteries", {0, 0}, QPixmap()));
        this->DeleteItem(A);
        this->DeleteItem(B);
        return true;
    }

    return false;
}


bool
Player::HasItemByName(const std::string& name) const
{
    for (Item* i : this->inventory)
    {
        if (i->GetName() == name) return true;
    }

    return false;
}

Player::~Player()
{
    for (Item* i : this->inventory)
    {
        delete i;
    }

    this->inventory.clear();
}

