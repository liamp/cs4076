#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include "item.h"
#include "game_object.h"

enum class Direction : unsigned char
{
    North = 0,
    South = 1,
    East  = 2,
    West  = 3,
};

class Room
{
private:
    std::string        name;
    std::vector<GameObject*> loaded_objects;
    std::string        locked_code;
    Room*              exits[4];
    bool               locked[4];

public:
    Room(std::string name);
    ~Room();
    Room*              GetAdjacent(Direction exit_direction) const;
    const std::string& GetName() const;
    std::vector<GameObject*> GetLoadedObjects() const;
    bool               IsLocked(Direction d) const;
    void               SetAdjacentRooms(Room* north, Room* south, Room* east, Room* west);
    void               AddObject(GameObject* item);
    void               RemoveObject(GameObject* item);
    void               SetLocked(Direction direction, std::string code);
    void               Unlock();
    const std::string& GetCode();

};

#endif // ROOM_H
