#ifndef TELEPORTER_H
#define TELEPORTER_H

#include "game_object.h"
#include "room.h"

class HiddenMessage : public GameObject
{
private:
    QAction* inspect_action;
    QAction* pull_action;
    bool     connected_inspect;
    bool     connected_pull;
    std::string secret_msg;

    void     ShowSecretMessage();
    void     ShowDescriptionBox();
public:
    HiddenMessage(MathUtils::vec2<int> pos, QPixmap sprite, std::string secret_msg);
    ~HiddenMessage() override;

    void GetActionsOnClick(QMenu* menu, QWidget* parent, Game* game_state) override;
};

#endif // TELEPORTER_H
