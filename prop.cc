#include <QMessageBox>
#include <QString>
#include <iostream>
#include "prop.h"

using std::string;
using MathUtils::vec2;


Prop::Prop(string name, string description, vec2<int> pos, QPixmap sprite, vec2<int> dimensions) : GameObject (name, description, pos, sprite, dimensions)
{
    this->inspect_action = new QAction("Inspect");
    this->connected = false;
}

void
Prop::GetActionsOnClick(QMenu* menu, QWidget* parent, Game*)
{
    menu->addAction(this->inspect_action);
    if (!this->connected)
    {
        parent->connect(this->inspect_action, &QAction::triggered, [this]() { this->ShowDescriptionBox(); });
        this->connected = true;
    }

}

void
Prop::ShowDescriptionBox() const
{
    QMessageBox msg;
    msg.setText(QString(this->description.c_str()));
    msg.exec();
}

Prop::~Prop()
{
    delete this->inspect_action;
}
